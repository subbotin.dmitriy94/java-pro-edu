package ru.study.java.pro.dto;

import jakarta.validation.constraints.NotBlank;

/**
 * UserDto.
 *
 * @author Dmitriy Subbotin
 */
public class UserDto {

  private Long id;

  @NotBlank(message = "Required filed 'username' not filled")
  private String username;

  public UserDto(Long id, String username) {
    this.id = id;
    this.username = username;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }
}
