package ru.study.java.pro.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.study.java.pro.dto.PaymentDto;
import ru.study.java.pro.service.ProductService;

/**
 * PaymentController.
 *
 * @author Dmitriy Subbotin
 */
@RestController
@RequestMapping("api/v1/payments")
public class PaymentController {

  private final ProductService productService;

  public PaymentController(ProductService productService) {
    this.productService = productService;
  }

  @PostMapping("/execute")
  public void executePayment(@RequestBody PaymentDto payment) {
    productService.executePayment(payment.getUserId(), payment.getProductId(), payment.getAmount());
  }
}
