package ru.study.java.pro.entity;

import ru.study.java.pro.enumiration.ProductType;

import java.math.BigDecimal;

/**
 * Product.
 *
 * @author Dmitriy Subbotin
 */
public class Product {

  private final Long id;

  private Long userId;

  private String accountNumber;

  private BigDecimal balance;

  private ProductType productType;

  public Product(Long id, Long userId, String accountNumber, BigDecimal balance,
      ProductType productType) {
    this.id = id;
    this.userId = userId;
    this.accountNumber = accountNumber;
    this.balance = balance;
    this.productType = productType;
  }

  public Long getId() {
    return id;
  }

  public Long getUserId() {
    return userId;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public ProductType getProductType() {
    return productType;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public void setProductType(ProductType productType) {
    this.productType = productType;
  }

  @Override
  public String toString() {
    return "Product{" +
        "id=" + id +
        ", userId=" + userId +
        ", accountNumber='" + accountNumber + '\'' +
        ", balance=" + balance +
        ", productType=" + productType +
        '}';
  }
}
