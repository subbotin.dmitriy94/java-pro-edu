package ru.study.java.pro.dto;

import java.util.List;

/**
 * PageDto.
 *
 * @author Dmitriy Subbotin
 */
public class PageDto<T>{

  private final List<T> content;

  public PageDto(List<T> content) {
    this.content = content;
  }

  public List<T> getContent() {
    return content;
  }
}
