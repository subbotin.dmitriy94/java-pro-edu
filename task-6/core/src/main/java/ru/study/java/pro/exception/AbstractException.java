package ru.study.java.pro.exception;

import ru.study.java.pro.enumiration.ErrorType;

/**
 * AbstractException.
 *
 * @author Dmitriy Subbotin
 */
public abstract class AbstractException extends RuntimeException {

  private final ErrorType code;

  private final String message;

  public ErrorType getCode() {
    return code;
  }

  @Override
  public String getMessage() {
    return message;
  }

  public AbstractException(ErrorType code, String message) {
    super(message);
    this.code = code;
    this.message = message;
  }
}
