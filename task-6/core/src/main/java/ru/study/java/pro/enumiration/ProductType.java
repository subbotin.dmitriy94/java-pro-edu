package ru.study.java.pro.enumiration;

/**
 * ProductType.
 *
 * @author Dmitriy Subbotin
 */
public enum ProductType {

  ACCOUNT,
  CREDIT_CARD,
  DEBT_CARD,
}
