package ru.study.java.pro.mapper;

import ru.study.java.pro.dto.ProductDto;
import ru.study.java.pro.entity.Product;

/**
 * ProductMapper.
 *
 * @author Dmitriy Subbotin
 */
public final class ProductMapper {

  public static ProductDto entityToDto(Product entity) {
    return new ProductDto(
        entity.getId(),
        entity.getUserId(),
        entity.getAccountNumber(),
        entity.getBalance(),
        entity.getProductType());
  }

  public static Product dtoToEntity(ProductDto dto) {
    return new Product(
        dto.getId(),
        dto.getUserId(),
        dto.getAccountNumber(),
        dto.getBalance(),
        dto.getProductType());
  }
}
