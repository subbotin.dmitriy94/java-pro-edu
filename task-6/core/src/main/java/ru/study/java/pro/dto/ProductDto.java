package ru.study.java.pro.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import ru.study.java.pro.enumiration.ProductType;

import java.math.BigDecimal;

/**
 * ProductDto.
 *
 * @author Dmitriy Subbotin
 */
public class ProductDto {

  private Long id;

  @NotNull(message = "Required filed 'userId' not filled")
  private Long userId;

  @NotBlank(message = "Required filed 'accountNumber' not filled")
  private String accountNumber;

  @NotNull(message = "Required filed 'balance' not filled")
  private BigDecimal balance;

  @NotNull(message = "Required filed 'productType' not filled")
  private ProductType productType;

  public ProductDto(Long id, Long userId, String accountNumber, BigDecimal balance,
      ProductType productType) {
    this.id = id;
    this.userId = userId;
    this.accountNumber = accountNumber;
    this.balance = balance;
    this.productType = productType;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public ProductType getProductType() {
    return productType;
  }

  public void setProductType(ProductType productType) {
    this.productType = productType;
  }
}
