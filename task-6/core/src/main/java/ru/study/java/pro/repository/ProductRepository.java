package ru.study.java.pro.repository;

import ru.study.java.pro.entity.Product;

import java.util.List;

/**
 * ProductRepository.
 *
 * @author Dmitriy Subbotin
 */
public interface ProductRepository {


  Product findById(Long id);
  Product findByIdAndUserId(Long id, Long userId);

  List<Product> findByUserId(Long userId);

  boolean delete(Product product);

  boolean update(Product oldProduct, Product newProduct);

  boolean add(Product product);
}
