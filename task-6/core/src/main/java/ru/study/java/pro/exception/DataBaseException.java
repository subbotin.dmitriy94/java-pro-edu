package ru.study.java.pro.exception;

import ru.study.java.pro.enumiration.ErrorType;

/**
 * DataBaseException.
 *
 * @author Dmitriy Subbotin
 */
public class DataBaseException extends AbstractException {

  public DataBaseException(ErrorType code, String message) {
    super(code, message);
  }
}
