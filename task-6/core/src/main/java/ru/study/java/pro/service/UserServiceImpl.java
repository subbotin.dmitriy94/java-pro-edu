package ru.study.java.pro.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.study.java.pro.entity.User;
import ru.study.java.pro.enumiration.ErrorType;
import ru.study.java.pro.exception.BadRequestException;
import ru.study.java.pro.exception.DataBaseException;
import ru.study.java.pro.exception.NotFoundException;
import ru.study.java.pro.repository.UserRepository;

import java.util.List;
import java.util.Objects;

/**
 * UserServiceImpl.
 *
 * @author Dmitriy Subbotin
 */
@Service
public class UserServiceImpl implements UserService {

  private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

  private final UserRepository userRepository;

  public UserServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public void addUser(String name) {
    if (userRepository.add(new User(name))) {
      log.info("User '{}' added", name);
    } else {
      log.warn("Error created user: '{}'", name);
    }
  }

  @Override
  public void deleteUser(String name) {
    if (userRepository.delete(getUserByName(name))) {
      log.info("User " + name + " deleted");
    } else {
      log.warn("Error deleted user '{}'", name);
      throw new DataBaseException(ErrorType.INTERNAL_ERROR, "User '" + name + "' not delete");
    }
  }

  @Override
  public User getUserByName(String name) {
    User user = userRepository.findByName(name);
    if (Objects.isNull(user)) {
      throw new NotFoundException(ErrorType.NOT_FOUND, "User '" + name + "' not found");
    }
    return user;
  }

  @Override
  public User getUserById(Long id) {
    User user = userRepository.findById(id);
    if (Objects.isNull(user)) {
      throw new NotFoundException(ErrorType.NOT_FOUND, "User by id '" + id + "' not found");
    }
    return user;
  }

  @Override
  public List<User> getAllUsers() {
    return userRepository.getAll();
  }

  @Override
  public void updateUser(User newUser) {
    if (Objects.isNull(newUser.getId())) {
      throw new BadRequestException(ErrorType.BAD_REQUEST, "Required filed 'id' not filled");
    }
    User oldUser = getUserById(newUser.getId());
    if (Objects.isNull(oldUser)) {
      throw new NotFoundException(ErrorType.NOT_FOUND, "User '" + newUser.getId() + "' not found");
    }
    if (userRepository.update(oldUser, new User(newUser.getUsername()))) {
      log.info("User '{}' updated to '{}'", oldUser.getUsername(), newUser.getUsername());
    } else {
      log.warn("Error updated user '{}'", oldUser.getUsername());
    }
  }
}
