package ru.study.java.pro.exception;

import ru.study.java.pro.enumiration.ErrorType;

/**
 * NotFoundException.
 *
 * @author Dmitriy Subbotin
 */
public class BadRequestException extends AbstractException {

  public BadRequestException(ErrorType code, String message) {
    super(code, message);
  }
}
