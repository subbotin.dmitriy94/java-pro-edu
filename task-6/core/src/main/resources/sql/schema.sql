CREATE TABLE IF NOT EXISTS public.users
(
    id       bigserial    NOT NULL,
    username varchar(255) NOT NULL,
    CONSTRAINT users_pk PRIMARY KEY (id),
    CONSTRAINT users_un UNIQUE (username)
);

CREATE TABLE IF NOT EXISTS public.products
(
    id             bigserial    NOT NULL,
    user_id        bigserial    NOT NULL,
    account_number varchar(255) NOT NULL,
    balance        varchar(20)  NOT NULL,
    product_type   varchar(20)  NOT NULL,
    CONSTRAINT products_pk PRIMARY KEY (id),
    CONSTRAINT products_un UNIQUE (account_number),
    CONSTRAINT products_fk FOREIGN KEY (user_id) REFERENCES public.users (id)
);