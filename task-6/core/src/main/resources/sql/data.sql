INSERT INTO public.users (id, username)
VALUES (1, 'admin')
ON CONFLICT DO NOTHING;

INSERT INTO public.users (id, username)
VALUES (2, 'manager')
ON CONFLICT DO NOTHING;

INSERT INTO public.users (id, username)
VALUES (3, 'developer')
ON CONFLICT DO NOTHING;

INSERT INTO public.users (id, username)
VALUES (4, 'director')
ON CONFLICT DO NOTHING;

INSERT INTO public.users (id, username)
VALUES (5, 'security')
ON CONFLICT DO NOTHING;

INSERT INTO public.products (id, user_id, account_number, balance, product_type)
VALUES(1, 4, '4745727216367373736', '100', 'CREDIT_CARD')
ON CONFLICT DO NOTHING;

INSERT INTO public.products (id, user_id, account_number, balance, product_type)
VALUES(2, 4, '324932949038728472', '1000', 'ACCOUNT')
ON CONFLICT DO NOTHING;