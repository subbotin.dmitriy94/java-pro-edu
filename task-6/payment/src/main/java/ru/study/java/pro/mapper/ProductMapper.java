package ru.study.java.pro.mapper;

import ru.study.java.pro.dto.ProductDto;
import ru.study.java.pro.integration.dto.ProductDtoRs;
import ru.study.java.pro.integration.dto.ProductInfoDtoRs;

import java.util.List;
import java.util.stream.Collectors;

/**
 * ProductMapper.
 *
 * @author Dmitriy Subbotin
 */
public final class ProductMapper {

  public static List<ProductDto> productsResponseToDto(ProductDtoRs response) {
    return response.getContent().stream()
        .map(p -> new ProductDto(
            p.getUserId(),
            p.getAccountNumber(),
            p.getBalance(),
            p.getProductType()))
        .collect(Collectors.toList());
  }

  public static ProductDto productInfoResponseToDto(ProductInfoDtoRs response) {
    return new ProductDto(
        response.getUserId(),
        response.getAccountNumber(),
        response.getBalance(),
        response.getProductType());
  }
}
