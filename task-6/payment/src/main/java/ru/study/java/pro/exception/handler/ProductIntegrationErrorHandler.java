package ru.study.java.pro.exception.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;
import ru.study.java.pro.dto.IntegrationErrorDto;
import ru.study.java.pro.enumiration.ErrorType;
import ru.study.java.pro.exception.ProductIntegrationException;

import java.io.IOException;

/**
 * ProductIntegrationErrorHandler.
 *
 * @author Dmitriy Subbotin
 */
@Component
public class ProductIntegrationErrorHandler implements ResponseErrorHandler {

  private final ObjectMapper objectMapper;

  public ProductIntegrationErrorHandler() {
    this.objectMapper = new ObjectMapper();
  }

  @Override
  public boolean hasError(ClientHttpResponse response) throws IOException {
    return response.getStatusCode().is4xxClientError() ||
        response.getStatusCode().is5xxServerError();
  }

  @Override
  public void handleError(ClientHttpResponse response) throws IOException {
    if (response.getStatusCode().isError()) {
      IntegrationErrorDto error =
          objectMapper.readValue(response.getBody(), IntegrationErrorDto.class);
      throw new ProductIntegrationException(
          ErrorType.INTEGRATION_ERROR,
          "Error integration product service",
          error);
    }
  }
}
