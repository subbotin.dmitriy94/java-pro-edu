package ru.study.java.pro.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.ConstructorBinding;

/**
 * ProductProperties.
 *
 * @author Dmitriy Subbotin
 */
@ConfigurationProperties(prefix = "integrations.product")
public class ProductProperties {

  private RestTemplateProperties client;

  public RestTemplateProperties getClient() {
    return client;
  }

  @ConstructorBinding
  public ProductProperties(RestTemplateProperties client) {
    this.client = client;
  }
}
