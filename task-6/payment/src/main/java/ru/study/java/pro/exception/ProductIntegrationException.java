package ru.study.java.pro.exception;

import ru.study.java.pro.dto.IntegrationErrorDto;
import ru.study.java.pro.enumiration.ErrorType;

/**
 * ProductIntegrationException.
 *
 * @author Dmitriy Subbotin
 */
public class ProductIntegrationException extends AbstractException {

  private IntegrationErrorDto error;

  public ProductIntegrationException(ErrorType code, String message, IntegrationErrorDto error) {
    super(code, message);
    this.error = error;
  }

  public IntegrationErrorDto getError() {
    return error;
  }

  public void setError(IntegrationErrorDto error) {
    this.error = error;
  }
}
