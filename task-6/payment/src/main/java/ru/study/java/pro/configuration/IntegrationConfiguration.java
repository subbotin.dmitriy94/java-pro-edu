package ru.study.java.pro.configuration;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import ru.study.java.pro.configuration.properties.ProductProperties;
import ru.study.java.pro.exception.handler.ProductIntegrationErrorHandler;
import ru.study.java.pro.integration.ProductIntegration;
import ru.study.java.pro.integration.ProductIntegrationImpl;

/**
 * IntegrationConfiguration.
 *
 * @author Dmitriy Subbotin
 */
@Configuration
public class IntegrationConfiguration {

  @Bean
  public ProductIntegration productIntegration(ProductProperties productProperties){
    RestTemplate restTemplate =
        new RestTemplateBuilder()
            .rootUri(productProperties.getClient().getUrl())
            .setConnectTimeout(productProperties.getClient().getReadTimeout())
            .setReadTimeout(productProperties.getClient().getConnectTimout())
            .errorHandler(new ProductIntegrationErrorHandler())
            .build();
    return new ProductIntegrationImpl(restTemplate);
  }
}
