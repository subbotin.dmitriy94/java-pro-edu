package ru.study.java.pro.configuration.properties;

import java.time.Duration;

/**
 * RestTemplateProperties.
 *
 * @author Dmitriy Subbotin
 */
public class RestTemplateProperties {

  private String url;
  private Duration connectTimout;
  private Duration readTimeout;

  public String getUrl() {
    return url;
  }

  public Duration getConnectTimout() {
    return connectTimout;
  }

  public Duration getReadTimeout() {
    return readTimeout;
  }

  public RestTemplateProperties(String url, Duration connectTimout, Duration readTimeout) {
    this.url = url;
    this.connectTimout = connectTimout;
    this.readTimeout = readTimeout;
  }
}
