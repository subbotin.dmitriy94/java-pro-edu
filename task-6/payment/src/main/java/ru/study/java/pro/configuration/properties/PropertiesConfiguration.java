package ru.study.java.pro.configuration.properties;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * PropertiesConfiguration.
 *
 * @author Dmitriy Subbotin
 */
@Configuration
@EnableConfigurationProperties({
    ProductProperties.class
})
public class PropertiesConfiguration {

}
