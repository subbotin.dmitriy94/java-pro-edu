package ru.study.java.pro.controller;

import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.study.java.pro.dto.PageDto;
import ru.study.java.pro.dto.PaymentDto;
import ru.study.java.pro.dto.ProductDto;
import ru.study.java.pro.service.ProductService;

/**
 * PaymentController.
 *
 * @author Dmitriy Subbotin
 */
@RestController
@RequestMapping("api/v1/payments")
public class PaymentController {

  private final ProductService productService;

  public PaymentController(ProductService productService) {
    this.productService = productService;
  }

  @GetMapping
  public PageDto<ProductDto> findProductsByUserId(@RequestHeader("USERID") Long userId) {
    return new PageDto<>(productService.getAllProductsByUserId(userId));
  }


  @GetMapping("/{productId}")
  public ProductDto findProductByUserId(
      @RequestHeader("USERID") Long userId,
      @PathVariable Long productId) {

    return productService.getProductByUserId(userId, productId);
  }

  @PostMapping("/transaction")
  public void executePayment(
      @RequestHeader("USERID") Long userId,
      @RequestBody @Valid PaymentDto payment) {

    productService.executePayment(userId, payment.getProductId(), payment.getAmount());
  }
}
