package ru.study.java.pro.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.study.java.pro.dto.ProductDto;
import ru.study.java.pro.integration.ProductIntegration;
import ru.study.java.pro.integration.dto.ProductDtoRs;
import ru.study.java.pro.integration.dto.ProductInfoDtoRs;
import ru.study.java.pro.mapper.ProductMapper;

import java.math.BigDecimal;
import java.util.List;

/**
 * ProductServiceImpl.
 *
 * @author Dmitriy Subbotin
 */
@Service
public class ProductServiceImpl implements ProductService {

  private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

  private final ProductIntegration productIntegration;

  public ProductServiceImpl(ProductIntegration productIntegration) {
    this.productIntegration = productIntegration;
  }

  @Override
  public List<ProductDto> getAllProductsByUserId(Long userId) {
    ProductDtoRs allProductsResponse = productIntegration.getProducts(userId);
    log.info("Success get products by userId '{}'", userId);
    return ProductMapper.productsResponseToDto(allProductsResponse);
  }

  @Override
  public ProductDto getProductByUserId(Long userId, Long productId) {
    ProductInfoDtoRs productInfoResponse = productIntegration.getProduct(userId, productId);
    log.info("Success get product by userId '{}', productId '{}'", userId, productId);
    return ProductMapper.productInfoResponseToDto(productInfoResponse);
  }

  @Override
  public void executePayment(Long userId, Long productId, BigDecimal paymentAmount) {
    productIntegration.executePayment(userId, productId, paymentAmount);
    log.info("Success execute payment by userId '{}', productId '{}'", userId, productId);
  }
}
