package ru.study.java.pro.dto;

import ru.study.java.pro.enumiration.ErrorType;

import java.time.LocalDateTime;

/**
 * ErrorDto.
 *
 * @author Dmitriy Subbotin
 */
public class ErrorDto {

  private final ErrorType code;

  private final String message;

  private final LocalDateTime timestamp;

  public ErrorDto(ErrorType code, String message) {
    this.code = code;
    this.message = message;
    this.timestamp = LocalDateTime.now();
  }

  public ErrorType getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public LocalDateTime getTimestamp() {
    return timestamp;
  }
}
