package ru.study.java.pro.service;

import ru.study.java.pro.dto.ProductDto;

import java.math.BigDecimal;
import java.util.List;

/**
 * ProductService.
 *
 * @author Dmitriy Subbotin
 */
public interface ProductService {

  List<ProductDto> getAllProductsByUserId(Long id);

  ProductDto getProductByUserId(Long userId, Long productId);

  void executePayment(Long userId, Long productId, BigDecimal paymentAmount);
}
