package ru.study.java.pro.integration;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.study.java.pro.integration.dto.PaymentDtoRq;
import ru.study.java.pro.integration.dto.ProductDtoRs;
import ru.study.java.pro.integration.dto.ProductInfoDtoRs;

import java.math.BigDecimal;

/**
 * ProductIntegrationImpl.
 *
 * @author Dmitriy Subbotin
 */
public class ProductIntegrationImpl implements ProductIntegration {

  private final RestTemplate restTemplate;

  public ProductIntegrationImpl(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Override
  public ProductDtoRs getProducts(Long userId) {
    ResponseEntity<ProductDtoRs> response =
        restTemplate.getForEntity("/products?userId=" + userId, ProductDtoRs.class);
    return response.getBody();
  }

  @Override
  public ProductInfoDtoRs getProduct(Long userId, Long productId) {
    ResponseEntity<ProductInfoDtoRs> response =
        restTemplate.getForEntity("/products/" + productId, ProductInfoDtoRs.class);
    return response.getBody();
  }

  @Override
  public void executePayment(Long userId, Long productId, BigDecimal paymentAmount) {
    PaymentDtoRq request = new PaymentDtoRq(userId, productId, paymentAmount);
    restTemplate.postForEntity("/payments/execute", request, String.class);
  }
}
