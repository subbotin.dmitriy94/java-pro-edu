package ru.study.java.pro.dto;

import java.math.BigDecimal;

/**
 * ProductDto.
 *
 * @author Dmitriy Subbotin
 */
public class ProductDto {

  private Long userId;

  private String accountNumber;

  private BigDecimal balance;

  private String productType;

  public ProductDto(Long userId, String accountNumber, BigDecimal balance, String productType) {
    this.userId = userId;
    this.accountNumber = accountNumber;
    this.balance = balance;
    this.productType = productType;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public String getProductType() {
    return productType;
  }

  public void setProductType(String productType) {
    this.productType = productType;
  }
}
