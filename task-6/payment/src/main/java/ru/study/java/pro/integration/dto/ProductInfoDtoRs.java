package ru.study.java.pro.integration.dto;

import java.math.BigDecimal;

/**
 * ProductInfoDtoRs.
 *
 * @author Dmitriy Subbotin
 */
public class ProductInfoDtoRs {

  private Long id;
  private Long userId;
  private String accountNumber;
  private BigDecimal balance;
  private String productType;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public String getProductType() {
    return productType;
  }

  public void setProductType(String productType) {
    this.productType = productType;
  }
}
