package ru.study.java.pro.dto;

import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;

/**
 * PaymentDto.
 *
 * @author Dmitriy Subbotin
 */
public class PaymentDto {

  @NotNull(message = "Required filed 'productId' not filled")
  private Long productId;

  @NotNull(message = "Required filed 'amount' not filled")
  private BigDecimal amount;

  public PaymentDto(Long productId, BigDecimal amount) {
    this.productId = productId;
    this.amount = amount;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }
}
