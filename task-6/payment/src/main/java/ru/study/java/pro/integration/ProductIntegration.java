package ru.study.java.pro.integration;

import ru.study.java.pro.integration.dto.ProductDtoRs;
import ru.study.java.pro.integration.dto.ProductInfoDtoRs;

import java.math.BigDecimal;

/**
 * ProductIntegration.
 *
 * @author Dmitriy Subbotin
 */
public interface ProductIntegration {


  ProductDtoRs getProducts(Long userId);

  ProductInfoDtoRs getProduct(Long userId, Long productId);

  void executePayment(Long userId, Long productId, BigDecimal paymentAmount);
}
