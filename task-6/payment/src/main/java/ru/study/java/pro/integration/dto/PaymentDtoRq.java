package ru.study.java.pro.integration.dto;

import java.math.BigDecimal;

/**
 * PaymentDtoRq.
 *
 * @author Dmitriy Subbotin
 */
public class PaymentDtoRq {

  private Long userId;
  private Long productId;

  private BigDecimal amount;

  public PaymentDtoRq(Long userId, Long productId, BigDecimal amount) {
    this.userId = userId;
    this.productId = productId;
    this.amount = amount;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }
}
