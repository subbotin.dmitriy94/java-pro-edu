package ru.study.java.pro.integration.dto;

import java.util.List;

/**
 * ProductDtoRs.
 *
 * @author Dmitriy Subbotin
 */
public class ProductDtoRs {

  private List<ProductInfoDtoRs> content;

  public List<ProductInfoDtoRs> getContent() {
    return content;
  }

  public void setContent(
      List<ProductInfoDtoRs> content) {
    this.content = content;
  }
}
