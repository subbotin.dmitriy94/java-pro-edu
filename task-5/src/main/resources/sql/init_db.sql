INSERT INTO public.users (id, username)
VALUES (nextval('public.users_id_seq'::regclass), 'admin')
ON CONFLICT DO NOTHING;

INSERT INTO public.users (id, username)
VALUES (nextval('public.users_id_seq'::regclass), 'manager')
ON CONFLICT DO NOTHING;

INSERT INTO public.users (id, username)
VALUES (nextval('public.users_id_seq'::regclass), 'developer')
ON CONFLICT DO NOTHING;

INSERT INTO public.users (id, username)
VALUES (nextval('public.users_id_seq'::regclass), 'director')
ON CONFLICT DO NOTHING;

INSERT INTO public.users (id, username)
VALUES (nextval('public.users_id_seq'::regclass), 'security')
ON CONFLICT DO NOTHING;

INSERT INTO public.products (id, user_id, account_number, balance, product_type)
VALUES(nextval('public.products_id_seq'::regclass), nextval('public.products_user_id_seq'::regclass), '4745727216367373736', '100', 'CREDIT_CARD')
ON CONFLICT DO NOTHING;

INSERT INTO public.products (id, user_id, account_number, balance, product_type)
VALUES(nextval('public.products_id_seq'::regclass), nextval('public.products_user_id_seq'::regclass), '324932949038728472', '1000', 'ACCOUNT')
ON CONFLICT DO NOTHING;