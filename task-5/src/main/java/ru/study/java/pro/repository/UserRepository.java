package ru.study.java.pro.repository;

import ru.study.java.pro.entity.User;

import java.util.List;

/**
 * UserRepository.
 *
 * @author Dmitriy Subbotin
 */
public interface UserRepository {

  boolean add(User entity);

  User findByName(String name);

  User findById(Long id);

  boolean update(User oldEntity, User newEntity);

  boolean delete(User entity);

  List<User> getAll();
}
