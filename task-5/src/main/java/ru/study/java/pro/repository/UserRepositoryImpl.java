package ru.study.java.pro.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.study.java.pro.entity.User;

import java.util.List;
import java.util.Map;

/**
 * UserRepositoryImpl.
 *
 * @author Dmitriy Subbotin
 */
@Repository
public class UserRepositoryImpl implements UserRepository {

  private final static String SELECT_ALL_USERS =
      "SELECT id, username "
          + "FROM users;";

  private final static String SELECT_ONE_USER_BY_NAME =
      "SELECT id, username "
          + "FROM users "
          + "WHERE username = :username;";

  private final static String SELECT_ONE_USER_BY_ID =
      "SELECT id, username "
          + "FROM users "
          + "WHERE id = :userId;";

  private final static String INSERT_USER =
      "INSERT INTO users "
          + "(username) "
          + "VALUES(:username);";

  private final static String DELETE_USER =
      "DELETE FROM users "
          + "WHERE username = :username;";

  private final static String UPDATE_USER =
      "UPDATE users "
          + "SET username = :newUsername "
          + "WHERE username = :oldUsername;";

  private static final Logger log = LoggerFactory.getLogger(UserRepositoryImpl.class);

  private final NamedParameterJdbcTemplate jdbcTemplate;

  private final RowMapper<User> userRowMapper;

  public UserRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate,
      RowMapper<User> userRowMapper) {
    this.jdbcTemplate = jdbcTemplate;
    this.userRowMapper = userRowMapper;
  }

  @Override
  public boolean add(User entity) {
    try {
      SqlParameterSource ps = new MapSqlParameterSource("username", entity.getUsername());
      jdbcTemplate.update(INSERT_USER, ps);
      return true;
    } catch (DataAccessException e) {
      log.error(e.getMessage());
      return false;
    }
  }

  @Override
  public User findByName(String name) {
    try {
      SqlParameterSource ps = new MapSqlParameterSource("username", name);
      return jdbcTemplate.queryForObject(SELECT_ONE_USER_BY_NAME, ps, userRowMapper);
    } catch (EmptyResultDataAccessException e) {
      log.error(e.getMessage());
      return null;
    }
  }

  @Override
  public User findById(Long id) {
    try {
      SqlParameterSource ps = new MapSqlParameterSource("userId", id);
      return jdbcTemplate.queryForObject(SELECT_ONE_USER_BY_ID, ps, userRowMapper);
    } catch (EmptyResultDataAccessException e) {
      log.error(e.getMessage());
      return null;
    }
  }

  @Override
  public boolean update(User oldEntity, User newEntity) {
    try {
      SqlParameterSource ps =
          new MapSqlParameterSource(
              Map.of(
                  "newUsername", newEntity.getUsername(),
                  "oldUsername", oldEntity.getUsername()));
      jdbcTemplate.update(UPDATE_USER, ps);
      return true;
    } catch (DataAccessException e) {
      log.error(e.getMessage());
      return false;
    }
  }

  @Override
  public boolean delete(User entity) {
    try {
      SqlParameterSource ps = new MapSqlParameterSource("username", entity.getUsername());
      jdbcTemplate.update(DELETE_USER, ps);
      return true;
    } catch (DataAccessException e) {
      log.error(e.getMessage());
      return false;
    }
  }

  @Override
  public List<User> getAll() {
    return jdbcTemplate.query(SELECT_ALL_USERS, userRowMapper);
  }
}
