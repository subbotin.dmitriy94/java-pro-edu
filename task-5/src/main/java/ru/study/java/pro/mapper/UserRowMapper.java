package ru.study.java.pro.mapper;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.study.java.pro.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * UserRowMapper.
 *
 * @author Dmitriy Subbotin
 */
@Component
public class UserRowMapper implements RowMapper<User> {

  @Override
  public User mapRow(ResultSet rs, int rowNum) throws SQLException {
    Long id = rs.getLong("id");
    String username = rs.getString("username");
    return new User(id, username);
  }
}
