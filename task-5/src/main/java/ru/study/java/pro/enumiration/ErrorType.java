package ru.study.java.pro.enumiration;

/**
 * ErrorType.
 *
 * @author Dmitriy Subbotin
 */
public enum ErrorType {

  NOT_FOUND,
  BAD_REQUEST,
  ERROR
}
