package ru.study.java.pro.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.study.java.pro.entity.Product;
import ru.study.java.pro.enumiration.ErrorType;
import ru.study.java.pro.exception.BadRequestException;
import ru.study.java.pro.exception.DataBaseException;
import ru.study.java.pro.exception.NotFoundException;
import ru.study.java.pro.repository.ProductRepository;

import java.util.List;
import java.util.Objects;

/**
 * ProductServiceImpl.
 *
 * @author Dmitriy Subbotin
 */

@Service
public class ProductServiceImpl implements ProductService {

  private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);
  private final ProductRepository productRepository;

  public ProductServiceImpl(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }

  @Override
  public Product getProductById(Long id) {
    return productRepository.findById(id);
  }

  @Override
  public List<Product> getProductsByUserId(Long userId) {
    return productRepository.findByUserId(userId);
  }

  @Override
  public void deleteProductById(Long id) {
    Product deleteProduct = getProductById(id);
    if (Objects.isNull(deleteProduct)) {
      throw new NotFoundException(ErrorType.NOT_FOUND, "Product '" + id + "' for delete not found");
    }
    if (productRepository.delete(deleteProduct)) {
      log.info("Product by id '{}' deleted", id);
    } else {
      log.warn("Error deleted product '{}'", id);
      throw new DataBaseException(ErrorType.ERROR, "Product '" + id + "' not delete");
    }
  }

  @Override
  public void addProduct(Product product) {
    if (productRepository.add(product)) {
      log.info("Product '{}' added", product);
    } else {
      throw new DataBaseException(ErrorType.ERROR, "Error created product: '" + product + "'");
    }
  }

  @Override
  public void updateProduct(Product product) {
    Long id = product.getId();
    Product oldProduct = getProductById(id);
    if (Objects.isNull(oldProduct)) {
      throw new BadRequestException(ErrorType.BAD_REQUEST, "Product '" + id + "' for update not found");
    }
    if (productRepository.update(oldProduct, product)) {
      log.info("Product '{}' updated to '{}'", oldProduct, product);
    } else {
      throw new DataBaseException(ErrorType.ERROR, "Error updated product '" + oldProduct + "'");
    }
  }
}
