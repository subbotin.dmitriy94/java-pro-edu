package ru.study.java.pro.exception;

import ru.study.java.pro.enumiration.ErrorType;

/**
 * NotFoundException.
 *
 * @author Dmitriy Subbotin
 */
public class NotFoundException extends AbstractException {

  public NotFoundException(ErrorType code, String message) {
    super(code, message);
  }
}
