package ru.study.java.pro.mapper;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.study.java.pro.entity.Product;
import ru.study.java.pro.enumiration.ProductType;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * ProductRowMapper.
 *
 * @author Dmitriy Subbotin
 */
@Component
public class ProductRowMapper implements RowMapper<Product> {

  @Override
  public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
    Long id = rs.getLong("id");
    Long userId = rs.getLong("user_id");
    String accountNumber = rs.getString("account_number");
    BigDecimal balance = rs.getBigDecimal("balance");
    ProductType productType = ProductType.valueOf(rs.getString("product_type"));
    return new Product(id, userId, accountNumber, balance, productType);
  }
}
