package ru.study.java.pro.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.study.java.pro.entity.Product;

import java.util.List;
import java.util.Map;

/**
 * ProductRepositoryImpl.
 *
 * @author Dmitriy Subbotin
 */
@Repository
public class ProductRepositoryImpl implements ProductRepository {

  private final static String FIND_BY_ID =
      "SELECT id, user_id, account_number, balance, product_type "
          + "FROM products "
          + "WHERE id = :productId";
  private final static String FIND_BY_USER_ID =
      "SELECT id, user_id, account_number, balance, product_type "
          + "FROM products "
          + "WHERE user_id = :userId";

  private final static String INSERT_PRODUCT =
      "INSERT INTO products (user_id, account_number, balance, product_type) "
          + "VALUES(:userId, :accountNumber, :balance, :productType)";

  private final static String UPDATE_PRODUCT =
      "UPDATE products "
          + "SET user_id = :userId , "
          + "account_number = :accountNumber, "
          + "balance = :balance, "
          + "product_type = :productType "
          + "WHERE id = :productId";

  private final static String DELETE_PRODUCT =
      "DELETE FROM products "
          + "WHERE id = :productId;";

  private static final Logger log = LoggerFactory.getLogger(ProductRepositoryImpl.class);

  private final NamedParameterJdbcTemplate jdbcTemplate;

  private final RowMapper<Product> productRowMapper;

  public ProductRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate, RowMapper<Product> productRowMapper) {
    this.jdbcTemplate = jdbcTemplate;
    this.productRowMapper = productRowMapper;
  }

  @Override
  public Product findById(Long id) {
    try {
      SqlParameterSource ps = new MapSqlParameterSource("productId", id);
      return jdbcTemplate.queryForObject(FIND_BY_ID, ps, productRowMapper);
    } catch (EmptyResultDataAccessException e) {
      log.error(e.getMessage());
      return null;
    }
  }

  @Override
  public List<Product> findByUserId(Long userId) {
    SqlParameterSource ps = new MapSqlParameterSource("userId", userId);
    return jdbcTemplate.query(FIND_BY_USER_ID, ps, productRowMapper);
  }

  @Override
  public boolean add(Product product) {
    try {
      SqlParameterSource ps =
          new MapSqlParameterSource(
              Map.of(
                  "userId", product.getUserId(),
                  "accountNumber", product.getAccountNumber(),
                  "balance", product.getBalance(),
                  "productType", product.getProductType().name()));
      jdbcTemplate.update(INSERT_PRODUCT, ps);
      return true;
    } catch (DataAccessException e) {
      e.printStackTrace();
      return false;
    }
  }

  @Override
  public boolean update(Product oldProduct, Product newProduct) {
    try {
      SqlParameterSource ps =
          new MapSqlParameterSource(
              Map.of(
                  "userId", newProduct.getUserId(),
                  "accountNumber", newProduct.getAccountNumber(),
                  "balance", newProduct.getBalance(),
                  "productType", newProduct.getProductType().name(),
                  "productId", oldProduct.getId()));

      jdbcTemplate.update(UPDATE_PRODUCT, ps);
      return true;
    } catch (DataAccessException e) {
      log.error(e.getMessage());
      return false;
    }
  }

  @Override
  public boolean delete(Product product) {
    try {
      SqlParameterSource ps = new MapSqlParameterSource("productId", product.getId());
      jdbcTemplate.update(DELETE_PRODUCT, ps);
      return true;
    } catch (DataAccessException e) {
      log.error(e.getMessage());
      return false;
    }
  }
}
