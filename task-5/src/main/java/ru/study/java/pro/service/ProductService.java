package ru.study.java.pro.service;

import ru.study.java.pro.entity.Product;

import java.util.List;

/**
 * ProductService.
 *
 * @author Dmitriy Subbotin
 */
public interface ProductService {

  Product getProductById(Long id);

  List<Product> getProductsByUserId(Long userId);

  void updateProduct(Product product);

  void deleteProductById(Long productId);

  void addProduct(Product product);
}
