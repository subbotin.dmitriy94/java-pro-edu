package ru.study.java.pro.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.study.java.pro.dto.ProductDto;
import ru.study.java.pro.entity.Product;
import ru.study.java.pro.mapper.ProductMapper;
import ru.study.java.pro.service.ProductService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * ProductController.
 *
 * @author Dmitriy Subbotin
 */
@RestController
@RequestMapping("api/v1/products")
public class ProductController {

  private final ProductService productService;

  public ProductController(ProductService productService) {
    this.productService = productService;
  }

  @GetMapping("/{id}")
  public ProductDto findById(@PathVariable Long id) {
    Product product = productService.getProductById(id);
    return ProductMapper.entityToDto(product);
  }

  @GetMapping
  public List<ProductDto> findAllByUserId(@RequestParam Long userId) {
    return productService.getProductsByUserId(userId)
        .stream()
        .map(ProductMapper::entityToDto)
        .collect(Collectors.toList());
  }

  @PostMapping
  public void addProduct(@RequestBody ProductDto newProduct) {
    productService.addProduct(ProductMapper.dtoToEntity(newProduct));
  }

  @PutMapping
  public void update(@RequestBody ProductDto productDto) {
    productService.updateProduct(ProductMapper.dtoToEntity(productDto));
  }

  @DeleteMapping("/{productId}")
  public void delete(@PathVariable Long productId) {
    productService.deleteProductById(productId);
  }
}
