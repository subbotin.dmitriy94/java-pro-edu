package ru.study.java.pro.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.study.java.pro.dto.UserDto;
import ru.study.java.pro.entity.User;
import ru.study.java.pro.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * UserController.
 *
 * @author Dmitriy Subbotin
 */
@RestController
@RequestMapping("api/v1/users")
public class UserController {

  private final UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("/{id}")
  public UserDto findById(@PathVariable Long id) {
    User user = userService.getUserById(id);
    return new UserDto(user.getId(), user.getUsername());
  }

  @GetMapping
  public List<UserDto> findAll() {
    return userService.getAllUsers()
        .stream()
        .map(u -> new UserDto(u.getId(), u.getUsername()))
        .collect(Collectors.toList());
  }

  @PostMapping
  public void addUser(@RequestBody UserDto newUser) {
    userService.addUser(newUser.getUsername());
  }

  @PutMapping
  public void update(@RequestBody UserDto newUser) {
    userService.updateUser(new User(newUser.getId(), newUser.getUsername()));
  }

  @DeleteMapping("/{username}")
  public void delete(@PathVariable String username) {
    userService.deleteUser(username);
  }
}
