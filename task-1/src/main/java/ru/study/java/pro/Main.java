package ru.study.java.pro;

import ru.study.java.pro.test.runner.TestRunner;
import ru.study.java.pro.test.service.UserServiceTest;

/**
 * Main.
 *
 * @author Dmitriy Subbotin
 */
public class Main {

  public static void main(String[] args) {
    TestRunner.runTests(UserServiceTest.class);
  }
}