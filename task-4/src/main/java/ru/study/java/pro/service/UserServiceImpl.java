package ru.study.java.pro.service;

import org.springframework.stereotype.Service;
import ru.study.java.pro.repository.BaseDao;
import ru.study.java.pro.entity.User;
import ru.study.java.pro.exception.DataBaseException;

import java.util.List;
import java.util.Objects;

/**
 * UserServiceImpl.
 *
 * @author Dmitriy Subbotin
 */
@Service
public class UserServiceImpl implements UserService {

  private final BaseDao<User> userDao;

  public UserServiceImpl(BaseDao<User> userDao) {
    this.userDao = userDao;
  }

  @Override
  public void addUser(String name) {
    if (userDao.add(new User(name))) {
      System.out.println("User " + name + " added");
    } else {
      System.err.println("Error created user: '" + name + "'");
    }
  }

  @Override
  public void deleteUser(String name) {
    if (userDao.delete(getUser(name))) {
      System.out.println("User " + name + " deleted");
    } else {
      System.err.println("Error deleted user '" + name + "'");
    }
  }

  @Override
  public User getUser(String name) {
    User user = userDao.get(name);
    if (Objects.isNull(user)) {
      throw new DataBaseException("User '" + name + "' not found");
    }
    return user;
  }

  @Override
  public List<User> getAllUsers() {
    return userDao.getAll();
  }

  @Override
  public void updateUser(String oldUsername, String newUsername) {
    if (userDao.update(getUser(oldUsername), new User(newUsername))) {
      System.out.println("User " + oldUsername + " updated");
    } else {
      System.err.println("Error updated user '" + oldUsername + "'");
    }
  }
}
