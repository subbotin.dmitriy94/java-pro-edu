package ru.study.java.pro.exception;

/**
 * DataBaseException.
 *
 * @author Dmitriy Subbotin
 */
public class DataBaseException extends AbstractException {

  public DataBaseException(String message) {
    super(message);
  }
}
