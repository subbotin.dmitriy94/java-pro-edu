package ru.study.java.pro.repository;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.study.java.pro.entity.User;

import java.util.List;

/**
 * UserDaoImpl.
 *
 * @author Dmitriy Subbotin
 */
@Repository
public class UserDaoImpl implements BaseDao<User> {

  private final static String SELECT_ALL_USERS =
      "SELECT id, username "
          + "FROM users;";

  private final static String SELECT_ONE_USER =
      "SELECT id, username "
          + "FROM users "
          + "WHERE username = ?;";

  private final static String INSERT_USER =
      "INSERT INTO users "
          + "(username) "
          + "VALUES(?);";

  private final static String DELETE_USER =
      "DELETE FROM users "
          + "WHERE username = ?;";

  private final static String UPDATE_USER =
      "UPDATE users "
          + "SET username = ? "
          + "WHERE username = ?;";

  private final JdbcTemplate jdbcTemplate;

  private final RowMapper<User> userRowMapper;

  public UserDaoImpl(JdbcTemplate jdbcTemplate, RowMapper<User> userRowMapper) {
    this.jdbcTemplate = jdbcTemplate;
    this.userRowMapper = userRowMapper;
  }

  @Override
  public boolean add(User entity) {
    try {
      jdbcTemplate.update(INSERT_USER, entity.getUsername());
      return true;
    } catch (DataAccessException e) {
      System.err.println(e.getMessage());
      return false;
    }
  }

  @Override
  public User get(String name) {
    try {
      return jdbcTemplate.queryForObject(SELECT_ONE_USER, userRowMapper, name);
    } catch (EmptyResultDataAccessException e) {
      System.err.println(e.getMessage());
      return null;
    }
  }

  @Override
  public boolean update(User oldEntity, User newEntity) {
    try {
      jdbcTemplate.update(UPDATE_USER, newEntity.getUsername(), oldEntity.getUsername());
      return true;
    } catch (DataAccessException e) {
      System.err.println(e.getMessage());
      return false;
    }
  }

  @Override
  public boolean delete(User entity) {
    try {
      jdbcTemplate.update(DELETE_USER, entity.getUsername());
      return true;
    } catch (DataAccessException e) {
      System.err.println(e.getMessage());
      return false;
    }
  }

  @Override
  public List<User> getAll() {
    return jdbcTemplate.query(SELECT_ALL_USERS, userRowMapper);
  }
}
