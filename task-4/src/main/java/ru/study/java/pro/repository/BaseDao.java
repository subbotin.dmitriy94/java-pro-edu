package ru.study.java.pro.repository;

import java.util.List;

/**
 * BaseDao.
 *
 * @author Dmitriy Subbotin
 */
public interface BaseDao<T> {

  boolean add(T entity);

  T get(String name);

  boolean update(T oldEntity, T newEntity);

  boolean delete(T entity);

  List<T> getAll();
}
