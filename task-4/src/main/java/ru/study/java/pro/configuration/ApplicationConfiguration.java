package ru.study.java.pro.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * ApplicationConfiguration.
 *
 * @author Dmitriy Subbotin
 */
@Configuration
@ComponentScan("ru.study.java.pro")
public class ApplicationConfiguration {

}
