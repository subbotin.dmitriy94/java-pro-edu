package ru.study.java.pro.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import ru.study.java.pro.configuration.property.ServiceProperties;

import javax.sql.DataSource;

/**
 * DataSourceConfiguration.
 *
 * @author Dmitriy Subbotin
 */
@Configuration
public class DataSourceConfiguration {

  private final ServiceProperties serviceProperties;

  public DataSourceConfiguration(ServiceProperties serviceProperties) {
    this.serviceProperties = serviceProperties;
  }

  @Bean
  DataSource dataSource() {
    HikariConfig hikariConfig = new HikariConfig();
    hikariConfig.setJdbcUrl(serviceProperties.getUrl());
    hikariConfig.setSchema(serviceProperties.getSchema());
    hikariConfig.setUsername(serviceProperties.getUsername());
    hikariConfig.setPassword(serviceProperties.getPassword());
    hikariConfig.setDriverClassName(serviceProperties.getDriveClassName());
    return new HikariDataSource(hikariConfig);
  }

  @Bean
  JdbcTemplate jdbcTemplate(DataSource dataSource) {
    return new JdbcTemplate(dataSource);
  }

  @Bean
  public DataSourceInitializer dataSourceInitializer(DataSource dataSource) {
    ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator();
    serviceProperties.getInitFiles()
        .forEach(file -> resourceDatabasePopulator.addScript(new ClassPathResource(file)));
    DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
    dataSourceInitializer.setDataSource(dataSource);
    dataSourceInitializer.setDatabasePopulator(resourceDatabasePopulator);
    return dataSourceInitializer;
  }
}
