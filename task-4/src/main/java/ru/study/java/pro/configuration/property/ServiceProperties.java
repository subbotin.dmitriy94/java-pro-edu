package ru.study.java.pro.configuration.property;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * ServiceProperties.
 *
 * @author Dmitriy Subbotin
 */
@Component
@Configuration
@PropertySource("classpath:application.yml")
public class ServiceProperties {

  @Value("${url}")
  private String url;

  @Value(("${schema}"))
  private String schema;

  @Value("${username}")
  private String username;

  @Value("${password}")
  private String password;

  @Value("${driver-class-name}")
  private String driveClassName;

  @Value("#{'${initialization-files}'.split(', ')}")
  private List<String> initFiles;

  public String getUrl() {
    return url;
  }

  public String getSchema() {
    return schema;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public String getDriveClassName() {
    return driveClassName;
  }

  public List<String> getInitFiles() {
    return initFiles;
  }
}
