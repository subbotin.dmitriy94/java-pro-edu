package ru.study.java.pro;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.study.java.pro.configuration.ApplicationConfiguration;
import ru.study.java.pro.exception.DataBaseException;
import ru.study.java.pro.service.UserService;
import ru.study.java.pro.service.UserServiceImpl;

/**
 * Task-4.
 *
 * @author Dmitriy Subbotin
 */
public class Main {

  public static void main(String[] args) {

    ApplicationContext applicationContext =
        new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
    UserService userService = applicationContext.getBean(UserServiceImpl.class);

    System.out.println("Get all users:");
    System.out.println(userService.getAllUsers());

    System.out.println("Add new user:");
    userService.addUser("doctor");

    System.out.println("Get user doctor");
    userService.getUser("doctor");

    System.out.println("Delete user director");
    userService.deleteUser("director");

    System.out.println("Update user doctor");
    userService.updateUser("doctor", "director");

    System.out.println("Add duplicate user admin");
    userService.addUser("admin");

    System.out.println("Get undefined user devops");
    try {
      System.out.println(userService.getUser("devops"));
    } catch (DataBaseException e) {
      System.err.println(e.getMessage());
    }

    System.out.println("Get all users");
    System.out.println(userService.getAllUsers());
  }
}