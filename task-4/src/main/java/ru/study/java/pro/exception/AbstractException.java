package ru.study.java.pro.exception;

/**
 * AbstractException.
 *
 * @author Dmitriy Subbotin
 */
public abstract class AbstractException extends RuntimeException {

  public AbstractException(String message) {
    super(message);
  }
}
