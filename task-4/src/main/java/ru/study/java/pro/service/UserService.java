package ru.study.java.pro.service;

import ru.study.java.pro.entity.User;

import java.util.List;

/**
 * UserService.
 *
 * @author Dmitriy Subbotin
 */
public interface UserService {

  void addUser(String name);

  void deleteUser(String name);

  User getUser(String name);

  List<User> getAllUsers();

  void updateUser(String oldUsername, String newUsername);
}
