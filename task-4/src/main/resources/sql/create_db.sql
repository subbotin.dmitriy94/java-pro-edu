CREATE TABLE IF NOT EXISTS public.users
(
    id       bigserial    NOT NULL,
    username varchar(255) NOT NULL,
    CONSTRAINT users_pk PRIMARY KEY (id),
    CONSTRAINT users_un UNIQUE (username)
);