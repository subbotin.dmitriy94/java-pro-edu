INSERT INTO public.users (id, username)
VALUES (nextval('public.users_id_seq'::regclass), 'admin')
ON CONFLICT DO NOTHING;

INSERT INTO public.users (id, username)
VALUES (nextval('public.users_id_seq'::regclass), 'manager')
ON CONFLICT DO NOTHING;

INSERT INTO public.users (id, username)
VALUES (nextval('public.users_id_seq'::regclass), 'developer')
ON CONFLICT DO NOTHING;

INSERT INTO public.users (id, username)
VALUES (nextval('public.users_id_seq'::regclass), 'director')
ON CONFLICT DO NOTHING;

INSERT INTO public.users (id, username)
VALUES (nextval('public.users_id_seq'::regclass), 'security')
ON CONFLICT DO NOTHING;