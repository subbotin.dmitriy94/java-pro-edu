package ru.study.java.pro;

import java.util.LinkedList;

/**
 * ThreadPoolCustom.
 *
 * @author Dmitriy Subbotin
 */
public class ThreadPoolCustom {

  private final Worker[] workers;

  private final LinkedList<Runnable> tasks;

  private boolean isShutdown = false;

  public ThreadPoolCustom(int countThreads) {
    if (countThreads <= 0) {
      throw new IllegalArgumentException("Count of threads mustn't be less or equal 0!");
    }
    workers = new Worker[countThreads];
    tasks = new LinkedList<>();

    for (int i = 0; i < countThreads; i++) {
      workers[i] = new Worker();
      workers[i].start();
    }
  }

  public synchronized void execute(Runnable runnable) {
    if (isShutdown) {
      throw new IllegalArgumentException("Thread pool is shutdown");
    }

    tasks.add(runnable);
    notify();
  }

  public synchronized void shutdown() {
    if (isShutdown) {
      return;
    }
    isShutdown = true;

    for (Worker w : workers) {
      w.interrupt();
    }
  }

  private class Worker extends Thread {

    @Override
    public void run() {
      Runnable runnable;

      while (true) {
        synchronized (ThreadPoolCustom.this) {
          while (tasks.isEmpty() && !isShutdown) {
            try {
              ThreadPoolCustom.this.wait();
            } catch (InterruptedException e) {
              return;
            }
          }

          runnable = tasks.removeFirst();
        }

        runnable.run();
      }
    }
  }
}
