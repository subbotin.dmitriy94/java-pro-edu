package ru.study.java.pro;

import java.util.List;

/**
 * Task-3.
 *
 * @author Dmitriy Subbotin
 */
public class Main {

  public static void main(String[] args) throws InterruptedException {

    ThreadPoolCustom threadPool = new ThreadPoolCustom(5);

    List<Runnable> tasks = List.of(
        getTask(1),
        getTask(2),
        getTask(3),
        getTask(4),
        getTask(5)
    );

    tasks.forEach(threadPool::execute);

    Thread.sleep(1000);

    threadPool.shutdown();
    System.out.println("Done");
  }

  private static Runnable getTask(int number) {
    return () -> {
      System.out.println("Task " + number + " starts");
      try {
        Thread.sleep((int) (100 * Math.random()));
        System.out.println("Task " + number + " finishes");
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    };
  }
}