package ru.study.java.pro.util;

import ru.study.java.pro.model.Person;
import ru.study.java.pro.model.Position;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * CollectionUtils.
 *
 * @author Dmitriy Subbotin
 */
public class CollectionUtils {

  /**
   * Удаление дубликатов
   */
  public static <T> List<T> deleteDuplicates(List<T> list) {
    return list.stream()
        .distinct()
        .collect(Collectors.toList());
  }

  /**
   * Удаление дубликатов с использованием Set
   */
  public static <T> Set<T> deleteDuplicatesToSet(List<T> list) {
    return new HashSet<>(list);
  }

  /**
   * Поиск третьего максимума
   */
  public static Integer getThirdMax(List<Integer> list) {
    return list.stream()
        .sorted(Comparator.reverseOrder())
        .skip(2)
        .findFirst()
        .orElseThrow(() -> new IllegalArgumentException("Less 3 elements in collection"));
  }

  /**
   * Поиск третьего уникального максимума
   */
  public static Integer getUniqueThirdMax(List<Integer> list) {
    return list.stream()
        .sorted(Comparator.reverseOrder())
        .distinct()
        .skip(2)
        .findFirst()
        .orElseThrow(() -> new IllegalArgumentException("Less 3 unique elements in collection"));
  }

  /**
   * Поиск трех самых старших инженеров
   */
  public static List<Person> getThreeOldestEngineers(List<Person> personList) {
    return personList.stream()
        .filter(p -> p.getPosition() == Position.ENGINEER)
        .sorted(Comparator.comparing(Person::getAge).reversed())
        .limit(3)
        .collect(Collectors.toList());
  }

  /**
   * Средний возраст инженеров
   */
  public static Double getAverageAgeOfEngineers(List<Person> personList) {
    return personList.stream()
        .filter(p -> p.getPosition() == Position.ENGINEER)
        .mapToInt(Person::getAge)
        .average()
        .orElseThrow(() -> new IllegalArgumentException("Engineer not found"));
  }

  /**
   * Поиск самого длинного слова
   */
  public static String getLongestWord(List<String> words) {
    return words.stream()
        .max(Comparator.comparingInt(String::length))
        .orElseThrow(() -> new IllegalArgumentException("Collection is empty"));
  }

  /**
   * Конвертирование строки в мапу, слово -> количество упоминаний
   */
  public static Map<String, Long> getWordCountMap(String s) {
    return Arrays.stream(s.split(" "))
        .collect(Collectors.groupingBy(w -> w, Collectors.counting()));
  }

  /**
   * Сортировка по длине, затем по алфавиту
   */
  public static List<String> sortByLengthThenByAlphabet(List<String> words) {
    return words.stream()
        .sorted(Comparator.comparingInt(String::length).thenComparing(Comparator.naturalOrder()))
        .collect(Collectors.toList());
  }

  /**
   * Поиск самого длинного слова в массиве предложений
   */
  public static String getLongestWord(String[] strings) {
    return Arrays.stream(strings)
        .flatMap(s -> Arrays.stream(s.split(" ")))
        .max(Comparator.comparing(String::length))
        .orElseThrow(() -> new IllegalArgumentException("Collection is empty"));
  }
}
