package ru.study.java.pro.model;

/**
 * Position.
 *
 * @author Dmitriy Subbotin
 */
public enum Position {

  MANAGER,
  DIRECTOR,
  ENGINEER
}
